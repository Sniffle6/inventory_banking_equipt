﻿using UnityEngine;
using System.Collections;
using MLSpace;

public class EnemyCombatBehaviour : StateMachineBehaviour {

    float timer = 0;
    bool hasHit;
    public Vector3[] spots0, spots1;
    public GameObject weapon;
    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        hasHit = false;
        timer = 0;
        spots0 = new Vector3[5];
        spots1 = new Vector3[5];
        weapon = animator.gameObject.GetComponent<CombatAI>().weapon;
    }
    //OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        timer += Time.deltaTime;
        if (timer >= .4f)
        {
            /*if (animator.GetComponent<CombatAI>().attackTarget.GetComponent<HealthManager>().health <= 0)
                return;
            BodyColliderScript bcs = animator.GetComponent<CombatAI>().attackTarget.GetComponent<RagdollManager>().getBodyPartInfo(2).collider.GetComponent<BodyColliderScript>();
            int[] parts = new int[] { bcs.index };
            bcs.ParentRagdollManager.StartHitReaction(parts, Vector3.back * 16.0f);*/
            for (var i = 0; i < weapon.transform.childCount; i++)
            {
                if (spots0[i] == Vector3.zero)
                {
                    spots0[i] = weapon.transform.GetChild(i).transform.position;
                    spots1[i] = spots0[i];
                }
                else
                {
                    spots1[i] = spots0[i];
                    spots0[i] = weapon.transform.GetChild(i).transform.position;
                }
                // Debug.Log(spots0[i] + ":" + spots1[i]);
                if (spots0[i] != Vector3.zero && spots1[i] != Vector3.zero)
                {
                    RaycastHit hit;
                    int mask = LayerMask.GetMask("ColliderLayer", "ColliderInactiveLayer");
                    if (Physics.Raycast(spots0[i], spots1[i] - spots0[i], out hit, 1, mask) && hit.collider.transform.root.tag == "Player" && !hasHit)
                    {
                        /*for (var o = 0; o < objsHit.Count; o++)
                        {
                            if (objsHit[o].name == hit.collider.name)
                                return;
                        }*/
                        BodyColliderScript bcs = hit.collider.GetComponent<BodyColliderScript>();
                        int[] parts = new int[] { bcs.index };
                        bcs.ParentRagdollManager.StartHitReaction(parts, Vector3.up * 16.0f);
                         hasHit = true;
                        // objsHit.Add(hit.collider);

                        var health = hit.transform.root.GetComponent<HealthManager>();
                        if (health == null)
                            return;
                        health.adjustHealth(-10);

                        Debug.DrawRay(spots0[i], spots1[i] - spots0[i] * 1, Color.green);
                        break;
                    }
                    else
                    {
                        Debug.DrawRay(spots0[i], spots1[i] - spots0[i] * 1, Color.red);
                    }
                }
            }
        }
    }

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    //override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
    //
    //}

    // OnStateMove is called right after Animator.OnAnimatorMove(). Code that processes and affects root motion should be implemented here
    //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
    //
    //}

    // OnStateIK is called right after Animator.OnAnimatorIK(). Code that sets up animation IK (inverse kinematics) should be implemented here.
    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
    //
    //}
}
