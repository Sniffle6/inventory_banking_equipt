﻿using UnityEngine;
using System.Collections;

public class CameraFollow : MonoBehaviour
{

    private GameObject target;
    private Camera myCam;

    // Use this for initialization
    void Start()
    {
        myCam = GetComponent<Camera>();
    }

    // Update is called once per frame
    void Update()
    {
      //  myCam.orthographicSize = (Screen.height / 50f) / 4f;

        if (target)
        {
            transform.position = Vector3.Lerp(transform.position, target.transform.position, 0.1f) + new Vector3(-0f, 0.6f, -0.55f);
        }
        else
        {
            target = GameObject.FindGameObjectWithTag("Player");
        }
    }
}
