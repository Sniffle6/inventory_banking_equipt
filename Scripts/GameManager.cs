﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameManager : MonoBehaviour
{
    ItemLoader itemStats;
    public static GameManager instance = null;
    TextAsset txt;
    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != null)
        {
            if (instance != this)
                Destroy(gameObject);
        }
        DontDestroyOnLoad(gameObject);
    }
    public GameObject instantiate(GameObject obj)
    {
        var newObj = Instantiate(obj);
        newObj.name = obj.name;
        return newObj;
    }
    public void destroy(GameObject obj)
    {
        Destroy(obj);
    }
    // Use this for initialization
    void Start()
    {
        txt = (TextAsset)Resources.Load("ItemStats", typeof(TextAsset));
        itemStats = new ItemLoader();
        string content = txt.text;
        itemStats.Load(content);
        ItemManager.Instance.setItemList();
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
