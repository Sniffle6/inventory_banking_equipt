﻿using UnityEngine;
using System.Collections;

public class CombatBehaviour : StateMachineBehaviour
{

    //OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        timer = 0;
    }

    float timer = 0;
    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        timer += Time.deltaTime;
        if(timer >= 0.1f && timer <= 0.125f)
        {
            Player.instance.GetComponent<Melee>().canHit = false;
        }
        if (timer >= 0.4f && Player.instance.GetComponent<Melee>().inState == 1)
        {
            Player.instance.GetComponent<Melee>().inState = 2;
            Player.instance.GetComponent<Melee>().canHit = true;
            Player.instance.GetComponent<Melee>().objsHit.Clear();
            Player.instance.GetComponent<Melee>().hasHit = false;
        }
        if(timer >= 1)
        {
            Player.instance.anim.SetInteger("combo", 0);
        }
    }

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        Player.instance.GetComponent<Melee>().inState = 0;
        Player.instance.GetComponent<Melee>().objsHit.Clear();
        Player.instance.GetComponent<Melee>().hasHit = false;
    }

    // OnStateMove is called right after Animator.OnAnimatorMove(). Code that processes and affects root motion should be implemented here
    //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
    //
    //}

    // OnStateIK is called right after Animator.OnAnimatorIK(). Code that sets up animation IK (inverse kinematics) should be implemented here.
    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
    //
    //}
}
