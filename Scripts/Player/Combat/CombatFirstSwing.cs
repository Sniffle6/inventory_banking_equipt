﻿using UnityEngine;
using System.Collections;

public class CombatFirstSwing : StateMachineBehaviour
{

    float timer = 0;
    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        Player.instance.GetComponent<Melee>().inState = 1;
        timer = 0;
        Player.instance.GetComponent<Melee>().canHit = false;
        Player.instance.GetComponent<Melee>().attacking = true;
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        timer += Time.deltaTime;
        if (timer >= 0.3f && Player.instance.GetComponent<Melee>().inState == 1)
        {
            Player.instance.GetComponent<Melee>().canHit = true;
        }
        if (timer >= 0.6f && Player.instance.GetComponent<Melee>().inState == 1)
        {
            Player.instance.anim.SetInteger("combo", 0);
        }
    }

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        Player.instance.anim.SetInteger("combo", 0);
        Player.instance.GetComponent<Melee>().inState = 0;
        Player.instance.GetComponent<Melee>().objsHit.Clear();
        Player.instance.GetComponent<Melee>().hasHit = false;
    }

    // OnStateMove is called right after Animator.OnAnimatorMove(). Code that processes and affects root motion should be implemented here
    //   override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
    //}

    // OnStateIK is called right after Animator.OnAnimatorIK(). Code that sets up animation IK (inverse kinematics) should be implemented here.
    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
    //
    //}
}
