﻿using UnityEngine;
using System.Collections.Generic;
using MLSpace;

public class Melee : MonoBehaviour
{

    Animator anim;
    public bool attacking;
    Vector3[] spots0, spots1;
    public int inState = 0;
    // Use this for initialization
    void Start()
    {
        anim = GetComponent<Animator>();
        anim.SetFloat("MoveAnimSpeed", 1f);
        spots0 = new Vector3[5];
        spots1 = new Vector3[5];
    }

    // Update is called once per frame
    void Update()
    {
        if (attacking && inState == 0)
        {
            anim.SetFloat("MoveAnimSpeed", 1f);
            anim.SetInteger("combo", 0);
            attacking = false;
            objsHit.Clear();
            hasHit = false;
        }/*
        if (attacking)
        {
            player.m_MoveSpeedMultiplier = Mathf.Lerp(1f, 0f, .2f);
            anim.SetFloat("MoveAnimSpeed", 0.70f);
            if (anim.GetFloat("Forward") > 0.7f)
                anim.SetFloat("Forward", 0.7f);
        }
        else
        {
            if (player.m_MoveSpeedMultiplier != 1)
            {
                player.m_MoveSpeedMultiplier = Mathf.Lerp(0.4f, 1f, 3);
            }
        }*/
    }
    public void combo()
    {
        if (!attacking && inState == 0)
        {
            canHit = true;
            anim.SetInteger("combo", 1);

        }
        else
        {
            if (inState == 1)
            {
                anim.SetInteger("combo", 2);
            }
        }
    }
    public List<Collider> objsHit = new List<Collider>();
    public HealthManager health;
    public bool hasHit = false;
    public bool canHit = true;
    void LateUpdate()
    {
        if (GetComponent<Melee>().attacking)
        {
            var weapon = Player.instance.GetComponent<Player>().Weapon_Slot.transform.GetChild(0);
            for (var i = 0; i < weapon.childCount; i++)
            {
                if (spots0[i] == Vector3.zero)
                {
                    spots0[i] = weapon.GetChild(i).transform.position;
                }
                else
                {
                    spots1[i] = spots0[i];
                    spots0[i] = weapon.GetChild(i).transform.position;
                }
                // Debug.Log(spots0[i] + ":" + spots1[i]);
                if (spots0[i] != Vector3.zero && spots1[i] != Vector3.zero)
                {
                    RaycastHit hit;
                    float ent = 1;
                    int mask = LayerMask.GetMask("ColliderLayer", "ColliderInactiveLayer");
                    if (Physics.Raycast(spots0[i], spots1[i] - spots0[i], out hit, ent, mask) && !hasHit && canHit)
                    {
                        if (hit.collider.tag == "Enemy")
                        {
                            for (var o = 0; o < objsHit.Count; o++)
                            {
                                if (objsHit[o].name == hit.collider.name)
                                    return;
                            }
                            BodyColliderScript bcs = hit.collider.GetComponent<BodyColliderScript>();
                            int[] parts = new int[] { bcs.index };
                            bcs.ParentRagdollManager.StartHitReaction(parts, Vector3.up * 16.0f);
                            hasHit = true;
                            objsHit.Add(hit.collider);

                            health = hit.transform.root.GetComponent<HealthManager>();
                            if (health == null)
                                return;
                            health.adjustHealth(-10);

                            break;
                        }
                        Debug.DrawRay(spots0[i], spots1[i] - spots0[i] * ent, Color.green);
                    }
                    else
                    {
                        Debug.DrawRay(spots0[i], spots1[i] - spots0[i] * ent, Color.red);
                    }
                }
            }
        }
    }
}
