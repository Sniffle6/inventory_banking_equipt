﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour
{
    public GameObject Weapon_Slot;
    public GameObject Shield_Slot;
    public Animator anim;

    public static Player instance = null;
    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != null)
        {
            if (instance != this)
                Destroy(gameObject);
        }
        DontDestroyOnLoad(gameObject);
    }
	// Use this for initialization
	void Start () {
        anim = GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
