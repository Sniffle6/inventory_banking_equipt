﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class Equipment
{
    private static readonly Equipment instance = new Equipment();
    static Equipment()
    {
    }
    private Equipment()
    {
    }
    public static Equipment Instance
    {
        get
        {
            return instance;
        }
    }
    public GameObject[] equippedItems = new GameObject[10];
    public List<GameObject> itemsOnTab = new List<GameObject>();

    GameObject equipmentTab()
    {
        var inv = InterfaceManager.Instance.findInterface("EquipmentTab");
        if (inv)
            return inv;
        else
            return null;
    }

    GameObject LegSlot()
    {
        return equipmentTab().transform.FindChild("Leg Slot").gameObject;
    }
    GameObject WeaponSlot()
    {
        return equipmentTab().transform.FindChild("Weapon Slot").gameObject;
    }
    GameObject ShieldSlot()
    {
        return equipmentTab().transform.FindChild("Shield Slot").gameObject;
    }
    GameObject HandsdSlot()
    {
        return equipmentTab().transform.FindChild("Hands Slot").gameObject;
    }
    GameObject BodySlot()
    {
        return equipmentTab().transform.FindChild("Body Slot").gameObject;
    }
    GameObject BootsSlot()
    {
        return equipmentTab().transform.FindChild("Boots Slot").gameObject;
    }
    GameObject HelmSlot()
    {
        return equipmentTab().transform.FindChild("Helm Slot").gameObject;
    }
    public void placeEquipmentOnPlayer(GameObject item)
    {
        switch (item.GetComponent<ItemData>()._Type)
        {
            case "Leggings":
                createBonedItem(item, 0);
                break;

            case "Weapon":
                createStationaryItem(item, 1, "Sword");
                break;

            case "Shield":
                createStationaryItem(item, 2, "Shield");
                break;

            case "Hands":
                createBonedItem(item, 3);
                break;

            case "Body":
                createBonedItem(item, 4);
                break;

            case "Boots":
                createBonedItem(item, 5);
                break;

            case "Helment":
                createBonedItem(item, 6);
                break;
        }
        openTab();
    }
    private void createBonedItem(GameObject item, int Slot)
    {
        if (equippedItems[Slot] != null)
            return;
        CreatePlayer.AddLimb((GameObject)Resources.Load("PlayerItems/" + item.name), Player.instance.gameObject, Slot);
        Inventory.Instance.removeItem(item);
    }
    private void createStationaryItem(GameObject item, int Slot, string anim)
    {
        if (equippedItems[Slot] != null)
            return;
        var obj = GameManager.instance.instantiate((GameObject)Resources.Load("PlayerItems/" + item.name));
        if (Slot == 1)
        {
            obj.transform.SetParent(Player.instance.Weapon_Slot.transform);
        }
        if (Slot == 2)
        {
            obj.transform.SetParent(Player.instance.Shield_Slot.transform);
        }
        obj.transform.localPosition = new Vector3(0f, 0f, 0f);
        obj.transform.localRotation = Quaternion.Euler(new Vector3(0f, 0f, 0f));
        equippedItems[Slot] = obj;
        Inventory.Instance.removeItem(item);
        Player.instance.anim.SetBool(anim, true);
    }
    public void openTab()
    {
        if (equipmentTab())
        {
            if (equippedItems[0] != null)
            {
                placeEquipmentOnTab(LegSlot(), 0);
            }
            if (equippedItems[1] != null)
            {
                placeEquipmentOnTab(WeaponSlot(), 1);
            }
            if (equippedItems[2] != null)
            {
                placeEquipmentOnTab(ShieldSlot(), 2);
            }
            if (equippedItems[3] != null)
            {
                placeEquipmentOnTab(HandsdSlot(), 3);
            }
            if (equippedItems[4] != null)
            {
                placeEquipmentOnTab(BodySlot(), 4);
            }
            if (equippedItems[5] != null)
            {
                placeEquipmentOnTab(BootsSlot(), 5);
            }
            if (equippedItems[6] != null)
            {
                placeEquipmentOnTab(HelmSlot(), 6);
            }
        }
    }
    void placeEquipmentOnTab(GameObject parent, int i)
    {
        Debug.Log(equippedItems[i].name);
        var obj = GameManager.instance.instantiate((GameObject)Resources.Load("InventoryItems/" + equippedItems[i].name));
        obj.name = equippedItems[i].name;
        obj.transform.SetParent(parent.transform);
        obj.transform.localPosition = new Vector3(0f, 0f, 0f);
        obj.GetComponent<Button>().onClick.AddListener(() => { Instance.itemClicked(obj, i); });
        itemsOnTab.Add(obj);
    }
    void itemClicked(GameObject item, int Slot)
    {
        if (item.transform.parent.name == "Weapon Slot")
            Player.instance.anim.SetBool("Sword", false);
        if (item.transform.parent.name == "Shield Slot")
            Player.instance.anim.SetBool("Shield", false);
        Equipment.Instance.removeItem(Slot);
        for (var i = 0; i < item.transform.parent.childCount; i++)
        {
            GameManager.instance.destroy(item.transform.parent.GetChild(i).gameObject);
        }
        GameManager.instance.destroy(item);
        itemsOnTab.Remove(item);
    }
    public void removeItem(int Slot)
    {
        if (!equippedItems[Slot])
            return;
        Inventory.Instance.addItem(equippedItems[Slot].name, 1);
        GameManager.instance.destroy(equippedItems[Slot]);
        equippedItems[Slot] = null;
    }
}
