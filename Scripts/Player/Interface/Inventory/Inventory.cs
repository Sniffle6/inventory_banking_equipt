﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class Inventory
{
    private static readonly Inventory instance = new Inventory();
    static Inventory()
    {
    }
    private Inventory()
    {
    }
    public static Inventory Instance
    {
        get
        {
            return instance;
        }
    }
    public List<GameObject> itemsInInventory = new List<GameObject>();
    public List<Item> itemsToAdd = new List<Item>();


    const int X_SPACE_BETWEEN_ITEM = 55;
    const int Y_SPACE_BETWEEN_ITEM = 43;
    const int NUMBER_OF_COLUMN = 4;
    GameObject inventory()
    {
        var inv = InterfaceManager.Instance.findInterface("Inventory");
        if (inv)
            return inv;
        else
            return null;
    }


    public void openInventory()
    {
        ItemManager.Instance.placeItemsOnInventory(inventory(), Instance.itemsToAdd, Instance.itemsInInventory);
        ItemManager.Instance.sortItems(Instance.itemsInInventory, X_SPACE_BETWEEN_ITEM, Y_SPACE_BETWEEN_ITEM, NUMBER_OF_COLUMN);
    }
    internal void itemClicked(GameObject item)
    {
        if (Bank.Instance.bank() && Bank.Instance.bank().activeInHierarchy)
            bankItem(item);
        else
        {
            Equipment.Instance.placeEquipmentOnPlayer(item);
        }
    }
    internal void itemClicked(GameObject item, int amount)
    {
        bankItem(item, amount);
    }
    public void removeItem(GameObject item)
    {
        GameManager.instance.destroy(item);
        Instance.itemsInInventory.Remove(item);
        ItemManager.Instance.sortItems(Instance.itemsInInventory, X_SPACE_BETWEEN_ITEM, Y_SPACE_BETWEEN_ITEM, NUMBER_OF_COLUMN);
    }
    private void bankItem(GameObject item)
    {
        Bank.Instance.addItem(item.name, 1);
        item.GetComponent<ItemData>()._Amount -= 1;
        if (item.GetComponent<ItemData>()._Stackable)
        {
            item.transform.GetChild(0).GetComponent<Text>().text = "" + item.GetComponent<ItemData>()._Amount;
        }
        if (item.GetComponent<ItemData>()._Amount < 1)
        {
            removeItem(item);
        }
    }
    private void bankItem(GameObject item, int amount)
    {
        if (item.GetComponent<ItemData>()._Stackable)
        {
            if (amount > item.GetComponent<ItemData>()._Amount)
                amount = item.GetComponent<ItemData>()._Amount;
        }
        else
        {
            if (amount > findItemAmount(item.name))
                amount = findItemAmount(item.name);
        }
        Bank.Instance.addItem(item.name, amount);
        item.GetComponent<ItemData>()._Amount -= amount;
        if (item.transform.GetChild(0).GetComponent<Text>())
        {
            item.transform.GetChild(0).GetComponent<Text>().text = "" + item.GetComponent<ItemData>()._Amount;
        }
        if (item.GetComponent<ItemData>()._Amount < 1)
        {
            if (!item.GetComponent<ItemData>()._Stackable)
            {
                for (var i = 0; i < amount; i++)
                {
                    if (findItem(item.name))
                    {
                        var itemToDelete = findItem(item.name);
                        removeItem(itemToDelete);
                    }
                }
            }
            else
            {
                removeItem(item);
            }
        }
    }
    public GameObject findItem(string name)
    {
        for (var i = 0; i < Instance.itemsInInventory.Count; i++)
        {
            if (Instance.itemsInInventory[i].name == name)
            {
                return Instance.itemsInInventory[i];
            }
        }
        return null;
    }
    public int findItemAmount(string name)
    {
        var count = 0;
        for (var i = 0; i < Instance.itemsInInventory.Count; i++)
        {
            if (Instance.itemsInInventory[i].name == name)
            {
                count++;
            }
        }
        return count;
    }
    internal void addItem(string name, int amount)
    {
        for (var i = 0; i < Instance.itemsToAdd.Count; i++)
        {
            if (Instance.itemsToAdd[i].Name == name && Instance.itemsToAdd[i].Stackable)
            {
                Instance.itemsToAdd[i].Amount += amount;
                return;
            }
        }
        var item = ItemManager.Instance.findItemStats(name, amount);
        if (item == null)
            Debug.LogError("Item not found!"+name);
        /*
         * 
         * When you have a stackable item in your inventory and you add a stackable then a non stackable too itemsToAdd, after opening the inventory it doesnt detect that their is already an existing
         * stackable item of that type in your inventory, thinking it needs a place to put the already existing stackable item. Problem) You get told your inventory is full 1 item short of being full.
         * (Possibly +1 for each stacked item already in your inventory)
         * if(you have 4 stackable items and your adding 4 stackable items too itemsToAdd)
         * then(inventory only allows 28-4 items to be added)
         * if(you have 4 stackable items and your adding 1 stackable items too itemsToAdd)
         * then(inventory only allows 28-1 items to be added)
         * 
         * 
         * pretty sure i fixed it but still extensive test, report all bugs here for further review
         * */
        var count = 0;
        for (var i = 0; i < Instance.itemsToAdd.Count; i++)
        {
            if (Instance.itemsToAdd[i].Stackable && findItem(Instance.itemsToAdd[i].Name))
            {
                count++;
            }
        }
        if (!item.Stackable && Instance.itemsInInventory.Count + (Instance.itemsToAdd.Count - count) + amount > 28 || item.Stackable && !findItem(name) && Instance.itemsInInventory.Count + Instance.itemsToAdd.Count >= 28)
            return;
        if (!item.Stackable)
        {
            for (var i = 0; i < amount; i++)
            {
                item.Amount = 1;
                Instance.itemsToAdd.Add(item);
                if (inventory() && inventory().activeInHierarchy)
                {
                    ItemManager.Instance.placeItemsOnInventory(inventory(), Instance.itemsToAdd, Instance.itemsInInventory);
                    ItemManager.Instance.sortItems(Instance.itemsInInventory, X_SPACE_BETWEEN_ITEM, Y_SPACE_BETWEEN_ITEM, NUMBER_OF_COLUMN);
                }
            }
        }
        else
        {
            Instance.itemsToAdd.Add(item);
            if (!inventory())
                return;
            if (inventory().activeInHierarchy)
            {
                ItemManager.Instance.placeItemsOnInventory(inventory(), Instance.itemsToAdd, Instance.itemsInInventory);
                ItemManager.Instance.sortItems(Instance.itemsInInventory, X_SPACE_BETWEEN_ITEM, Y_SPACE_BETWEEN_ITEM, NUMBER_OF_COLUMN);
            }
        }
    }

}
