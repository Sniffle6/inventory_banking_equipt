﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

public class InterfaceManager
{
    private static readonly InterfaceManager instance = new InterfaceManager();
    static InterfaceManager()
    {
    }
    private InterfaceManager()
    {
    }
    public static InterfaceManager Instance
    {
        get
        {
            return instance;
        }
    }

    public List<GameObject> interfaces = new List<GameObject>();

    public bool isInterfaceInstantiated(string interfaceToCheck)
    {
        for (var i = 0; i < Instance.interfaces.Count; i++)
        {
            if (Instance.interfaces[i].name == interfaceToCheck)
            {
                return true;
            }
        }
        return false;
    }
    public bool isInterfaceOpen(string interfaceToCheck)
    {
        for (var i = 0; i < Instance.interfaces.Count; i++)
        {
            if (Instance.interfaces[i].name == interfaceToCheck)
            {
                if (instance.interfaces[i].activeInHierarchy)
                    return true;
            }
        }
        return false;
    }
    public void openCloseInterface(KeyCode input, string iface, float percentFromRightOfScreen, float percentFromTopOfScreen, Action a)
    {
        if (Input.GetKeyDown(input))
        {
            if (!InterfaceManager.Instance.isInterfaceInstantiated(iface))
            {
                var inventory = GameManager.instance.instantiate((GameObject)Resources.Load("Interfaces/" + iface));
                inventory.transform.SetParent(GameObject.FindGameObjectWithTag("Canvas").transform);
                inventory.name = iface;
                inventory.GetComponent<RectTransform>().position = new Vector3(Screen.width * percentFromRightOfScreen, Screen.height * percentFromTopOfScreen);
                InterfaceManager.Instance.interfaces.Add(inventory);
                a();
            }
            else
            {
                if (InterfaceManager.Instance.isInterfaceOpen(iface))
                    InterfaceManager.Instance.setInterfaceActive(iface, false);
                else
                {
                    InterfaceManager.Instance.setInterfaceActive(iface, true);
                    a();
                }

            }
        }
    }
    public GameObject findInterface(string interfaceToCheck)
    {
        for (var i = 0; i < Instance.interfaces.Count; i++)
        {
            if (Instance.interfaces[i].name == interfaceToCheck)
            {
                return Instance.interfaces[i];
            }
        }
        return null;
    }
    public void setInterfaceActive(string interfaceToSet, bool active)
    {
        for (var i = 0; i < Instance.interfaces.Count; i++)
        {
            if (Instance.interfaces[i].name == interfaceToSet)
            {
                instance.interfaces[i].SetActive(active);
            }
        }
    }

}
