﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class Bank
{
    private static readonly Bank instance = new Bank();
    static Bank()
    {
    }
    private Bank()
    {
    }
    public static Bank Instance
    {
        get
        {
            return instance;
        }
    }
    public List<GameObject> itemsInBank = new List<GameObject>();
    public List<Item> itemsToAdd = new List<Item>();

    public GameObject bank()
    {
        var inv = InterfaceManager.Instance.findInterface("Bank");
        if (inv)
            return inv;
        else
            return null;
    }
    internal void openBank()
    {
        ItemManager.Instance.placeItemsOnInventory(bank(), Instance.itemsToAdd, Instance.itemsInBank);
        ItemManager.Instance.sortItems(Instance.itemsInBank, 55, 43, 6);
    }
    public void itemClicked(GameObject item)
    {
        if (Inventory.Instance.itemsInInventory.Count+Inventory.Instance.itemsToAdd.Count > 27 && !item.GetComponent<ItemData>()._Stackable || !Inventory.Instance.findItem(item.name) && item.GetComponent<ItemData>()._Stackable && Inventory.Instance.itemsInInventory.Count > 27)
            return;
        Inventory.Instance.addItem(item.name, 1);
        item.GetComponent<ItemData>()._Amount -= 1;
        if (item.transform.GetChild(0).GetComponent<Text>())
        item.transform.GetChild(0).GetComponent<Text>().text = "" + item.GetComponent<ItemData>()._Amount;
        if (item.GetComponent<ItemData>()._Amount < 1)
        {
            GameManager.instance.destroy(item);
            Instance.itemsInBank.Remove(item);
            ItemManager.Instance.sortItems(Instance.itemsInBank, 55, 43, 6);
        }
    }
    public void itemClicked(GameObject item, int amount)
    {
        if (amount > item.GetComponent<ItemData>()._Amount)
            amount = item.GetComponent<ItemData>()._Amount;
        if (!item.GetComponent<ItemData>()._Stackable && Inventory.Instance.itemsInInventory.Count + amount + Inventory.Instance.itemsToAdd.Count > 28)
        {
            amount = 28 - (Inventory.Instance.itemsInInventory.Count + Inventory.Instance.itemsToAdd.Count);
        }
        if (amount < 0)
            amount = 0;
        if (item.GetComponent<ItemData>()._Stackable && !Inventory.Instance.findItem(item.name) && Inventory.Instance.itemsInInventory.Count > 27)
            return;
        Inventory.Instance.addItem(item.name, amount);
        item.GetComponent<ItemData>()._Amount -= amount;
        if (item.transform.GetChild(0).GetComponent<Text>())
            item.transform.GetChild(0).GetComponent<Text>().text = "" + item.GetComponent<ItemData>()._Amount;
        if (item.GetComponent<ItemData>()._Amount < 1)
        {
            GameManager.instance.destroy(item);
            Instance.itemsInBank.Remove(item);
            ItemManager.Instance.sortItems(Instance.itemsInBank, 55, 43, 6);
        }
    }
    internal void addItem(string name, int amount)
    {
        for (var i = 0; i < Instance.itemsToAdd.Count; i++)
        {
            if (Instance.itemsToAdd[i].Name == name)
            {
                Instance.itemsToAdd[i].Amount += amount;
                return;
            }
        }
        var item = ItemManager.Instance.findItemStats(name, amount);
        if (item == null)
            Debug.LogError("Item not found!");
        Instance.itemsToAdd.Add(item);
        if (!bank())
            return;
        if (bank().activeInHierarchy)
        {
            ItemManager.Instance.placeItemsOnInventory(bank(), Instance.itemsToAdd, Instance.itemsInBank);
            ItemManager.Instance.sortItems(Instance.itemsInBank, 55, 43, 6);
        }
    }
}
