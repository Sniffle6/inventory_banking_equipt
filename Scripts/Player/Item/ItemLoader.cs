﻿using UnityEngine;
using System.Collections;
using System.Text;
using System.IO;
using System;

public class ItemLoader 
{
    public bool Load(string fileName)
    {
        // Handle any problems that might arise when reading the text
/*


!How to use!
In your gamemanager script or any script anywhere call 
Load(string fileName)
if you wantthe file out of your resources folder just put the text file into resources and use this path



   	 ItemLoader itemStats;

        txt = (TextAsset)Resources.Load("ItemStats", typeof(TextAsset)); // 'ItemStats'is the file name
        itemStats = new ItemLoader();
        string content = txt.text;
        itemStats.Load(content);




*/
        try
        {
            string line;
            // Create a new StreamReader, tell it which file to read and what encoding the file
            // was saved as
            StringReader theReader = new StringReader(fileName);
            // Immediately clean up the reader after this block of code is done.
            // You generally use the "using" statement for potentially memory-intensive objects
            // instead of relying on garbage collection.
            // (Do not confuse this with the using directive for namespace at the 
            // beginning of a class!)
            using (theReader)
            {
                // While there's lines left in the text file, do this:
                do
                {
                    line = theReader.ReadLine();
                    if (line != null)
                    {
                        string[] entries = line.Split('\t');
                        if (entries.Length > 0)
                        {
                            Debug.Log(entries[0]);
                            Item stats = new Item(entries[0], entries[1], Convert.ToBoolean(entries[2]), 0, entries[3]);
                            ItemManager.Instance.ItemStats.Add(stats);
                        }
                    }
                }
                while (line != null);
                // Done reading, close the reader and return true to broadcast success    
                theReader.Close();
                return true;
            }
        }
        // If anything broke in the try block, we throw an exception with information
        // on what didn't work
        catch (Exception e)
        {
            Console.Write("{0}\n", e.Message);
            return false;
        }
    }
}

