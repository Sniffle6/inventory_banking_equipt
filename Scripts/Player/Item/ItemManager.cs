﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using System;

public class ItemManager
{

    private static readonly ItemManager instance = new ItemManager();
    static ItemManager()
    {
    }
    private ItemManager()
    {
    }
    public static ItemManager Instance
    {
        get
        {
            return instance;
        }
    }

    public List<UnityEngine.Object> itemList = new List<UnityEngine.Object>();
    public List<Item> ItemStats = new List<Item>();

    internal void setItemList()
    {
        UnityEngine.Object[] tempItems = Resources.LoadAll("InventoryItems", typeof(GameObject));
        for (var i = 0; i < tempItems.Length; i++)
        {
            var clone = tempItems[i];
            var item = clone;
            item.name = clone.name;
            itemList.Add(item);
            tempItems[i] = null;
            item = null;
            Resources.UnloadAsset(item);
            Resources.UnloadAsset(tempItems[i]);
            Resources.UnloadUnusedAssets();
        }
    }

    public Item findItemStats(string name, int amount)
    {
        for (var i = 0; i < Instance.ItemStats.Count; i++)
        {
            if (Instance.ItemStats[i].Name == name)
            {
                return CreateItem(Instance.ItemStats[i].Name, Instance.ItemStats[i].Type, Instance.ItemStats[i].Stackable, amount, Instance.ItemStats[i].Discription);
            }
        }
        return null;
    }

    private Item CreateItem(string name, string type, bool stackable, int amount, string discription)
    {
        Item item = new Item(name, type, stackable, amount, discription);
        return item;
    }
    public void sortItems(List<GameObject> itemsToSort, float X_SPACE_BETWEEN_ITEM, int Y_SPACE_BETWEEN_ITEM, int NUMBER_OF_COLUMN)
    {
        for (var i = 0; i < itemsToSort.Count; i++)
        {
            itemsToSort[i].transform.localPosition = new Vector3(X_SPACE_BETWEEN_ITEM * (i % NUMBER_OF_COLUMN), -Y_SPACE_BETWEEN_ITEM * (i / NUMBER_OF_COLUMN), 0f);
        }
    }
    public void placeItemsOnInventory(GameObject parent, List<Item> itemToAdd, List<GameObject> itemsAdded)
    {
        for (var o = 0; o < itemsAdded.Count; o++)
        {
            for (var i = 0; i < itemToAdd.Count; i++)
            {
                if ((itemToAdd[i].Name == itemsAdded[o].name && itemToAdd[i].Stackable) || parent.name == "Bank" && itemToAdd[i].Name == itemsAdded[o].name )
                {
                    itemsAdded[o].GetComponent<ItemData>()._Amount += itemToAdd[i].Amount;
                    if (itemsAdded[o].transform.childCount > 0)
                    {
                        itemsAdded[o].transform.GetChild(0).GetComponent<Text>().text = "" + itemsAdded[o].GetComponent<ItemData>()._Amount;
                        itemToAdd.Remove(itemToAdd[i]);
                        break;
                    }
                    else
                    {
                        Misc.createText(itemsAdded[o].transform, new Vector3(-20f, 20f, 0f), itemsAdded[o].GetComponent<ItemData>()._Amount);
                        itemToAdd.Remove(itemToAdd[i]);
                        break;
                    }
                }
            }
        }
        for (var i = 0; i < itemToAdd.Count; i++)
        {
            var clone = findPrefabItem(itemToAdd[i].Name);
            var item = GameManager.instance.instantiate(clone);
            item.transform.SetParent(parent.transform);
            item.name = itemToAdd[i].Name;
            var stats = item.GetComponent<ItemData>();
            stats._Amount = itemToAdd[i].Amount;
            stats._Discription = itemToAdd[i].Discription;
            stats._Name = itemToAdd[i].Name;
            stats._Type= itemToAdd[i].Type;
            stats._Stackable = itemToAdd[i].Stackable;
            if (stats._Stackable || parent.name == "Bank")
            {
                Misc.createText(item.transform, new Vector3(-20f, 20f, 0f), stats._Amount);
            }
            if (parent.name == "Inventory")
            {
                item.GetComponent<Button>().onClick.AddListener(() => { Inventory.Instance.itemClicked(item); });
            }
            else if (parent.name == "Bank")
            {
                item.GetComponent<Button>().onClick.AddListener(() => { Bank.Instance.itemClicked(item); });
            }
            itemsAdded.Add(item);
        }
        itemToAdd.Clear();
    }

    public GameObject findPrefabItem(string name)
    {
        for (var i = 0; i < itemList.Count; i++)
        {
            if (itemList[i].name == name)
            {
                var temp = (GameObject)itemList[i];
                return temp;
            }
        }
        return null;
    }
}
