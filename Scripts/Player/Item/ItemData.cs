﻿using UnityEngine;
using System.Collections;

public class ItemData : MonoBehaviour
{
    public string _Name;
    public string _Type;
    public int _Amount;
    public bool _Stackable;
    public string _Discription;
}
