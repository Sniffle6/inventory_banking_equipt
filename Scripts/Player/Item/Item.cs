﻿using System;

[Serializable]
public class Item
{
    public string Name;
    public bool Stackable;
    public int Amount;
    public string Type;
    public string Discription;
    public Item(string name, string type, bool stackable, int amount, string discription)
    {
        Name = name;
        Type = type;
        Stackable = stackable;
        Amount = amount;
        Discription = discription;
    }

}

