﻿using UnityEngine;
using System.Collections;

[DisallowMultipleComponent]
[RequireComponent(typeof(Animator))]
public class Movement : MonoBehaviour {
    Animator anim;
    bool isWalking = false;
    void Awake()
    {
        anim = GetComponent<Animator>();
    }


    void Update()
    {
        Turning();
        Walking();
        Move();
    }
    void Turning()
    {
        anim.SetFloat("Turning", Input.GetAxis("Horizontal"));
    }
    void Walking()
    {
        if (Input.GetKeyDown(KeyCode.LeftShift))
        {
            isWalking = !isWalking;
            anim.SetBool("Walk", isWalking);
        }
    }
    void Move()
    {
        anim.SetFloat("Forward",Input.GetAxis("Vertical"));
    }
}
