﻿using UnityEngine;
using System.Collections;

public class CreatePlayer
{

    public GameObject objPlayer;
    public GameObject objLimb;



    /*Update is called once per frame
    void Update()
    {
    }
    */

    public static void AddLimb(GameObject BonedObj, GameObject RootObj, int Slot)
    {
        var BonedObjects = BonedObj.gameObject.GetComponentsInChildren<SkinnedMeshRenderer>();
        GameObject parent = new GameObject();
        parent.transform.SetParent(RootObj.transform);
        parent.name = BonedObj.name;
        foreach (SkinnedMeshRenderer SkinnedRenderer in BonedObjects)
        {
            var limb = ProcessBonedObject(SkinnedRenderer, RootObj);
            limb.transform.SetParent(parent.transform);
            limb.name = BonedObj.name;
        }
        Equipment.Instance.equippedItems[Slot] = parent;
    }

    public static GameObject ProcessBonedObject(SkinnedMeshRenderer ThisRenderer, GameObject RootObj)
    {
        /*      Create the SubObject        */
        var NewObj = new GameObject(ThisRenderer.gameObject.name);
        NewObj.transform.parent = RootObj.transform;
        /*      Add the renderer        */
        NewObj.AddComponent<SkinnedMeshRenderer>();
        var NewRenderer = NewObj.GetComponent<SkinnedMeshRenderer>();
        /*      Assemble Bone Structure     */
        var MyBones = new Transform[ThisRenderer.bones.Length];
        for (var i = 0; i < ThisRenderer.bones.Length; i++)
            MyBones[i] = FindChildByName(ThisRenderer.bones[i].name, RootObj.transform);
        /*      Assemble Renderer       */
        NewRenderer.bones = MyBones;
        NewRenderer.sharedMesh = ThisRenderer.sharedMesh;
        NewRenderer.materials = ThisRenderer.sharedMaterials;
        return NewObj;
    }


    public static Transform FindChildByName(string ThisName, Transform ThisGObj)
    {
        Transform ReturnObj;
        if (ThisGObj.name == ThisName)
            return ThisGObj.transform;
        foreach (Transform child in ThisGObj)
        {
            ReturnObj = FindChildByName(ThisName, child);
            if (ReturnObj)
                return ReturnObj;
        }
        return null;
    }
}
