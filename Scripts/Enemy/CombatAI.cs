﻿using UnityEngine;
using System.Collections;
using System;
using MLSpace;

public class CombatAI : MonoBehaviour
{

    public GameObject attackTarget;
    public float distanceToAttack;
    private RagdollManager ragdoll;
    private RagdollManager targetRagdoll;
    public GameObject weapon;
    Animator anim;

    // Use this for initialization
    void Start()
    {
        anim = GetComponent<Animator>();
        ragdoll = GetComponent<RagdollManager>();
        distanceToAttack = 2f;
        searchForTarget();
        setWalkingPath();
        setMoveSpeed();
        inRangeToAttack();
    }

    private void searchForTarget()
    {
        attackTarget = Player.instance.gameObject;
        targetRagdoll = attackTarget.GetComponent<RagdollManager>();
    }
    private void setWalkingPath()
    {
        GetComponent<AIPath>().target = attackTarget.transform;
    }
    private float distanceFromPlayer()
    {
        return Vector3.Distance(gameObject.transform.position, attackTarget.transform.position);
    }
    private bool inRangeToAttack()
    {
        if (distanceFromPlayer() < distanceToAttack)
        {
            return true;
        }
        return false;
    }
    private void setMoveSpeed()
    {
        if (distanceFromPlayer() > 3)
        {
            anim.SetBool("Charge", true);
            GetComponent<AIPath>().speed = 5;
        }
        else
        {
            anim.SetBool("Charge", false);
            GetComponent<AIPath>().speed = 2;
        }
    }
    float ent = 1;
    // Update is called once per frame
    void LateUpdate()
    {
        setMoveSpeed();
        if (inRangeToAttack()
            && targetRagdoll.State != RagdollManager.RagdollState.Ragdoll
            && targetRagdoll.State != RagdollManager.RagdollState.GettingUpAnim
            && targetRagdoll.State != RagdollManager.RagdollState.Blend
            && ragdoll.State != RagdollManager.RagdollState.Blend
            && ragdoll.State != RagdollManager.RagdollState.GettingUpAnim
            && ragdoll.State != RagdollManager.RagdollState.Ragdoll)
        {
            anim.SetTrigger("Tattack");
        }
    }
}
