﻿using UnityEngine;
using System.Collections;
using MLSpace;
public class HealthManager : MonoBehaviour {
    public int health;
    // Use this for initialization
    void Start () {
        health = 100;
    }
	
	// Update is called once per frame
	void Update () {
	
	}

   public void adjustHealth(int adjust)
    {
        health += adjust;
        if (health <= 0)
        {
            GetComponent<RagdollManager>().StartRagdoll();
        }
    }
}
