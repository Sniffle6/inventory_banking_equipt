﻿using UnityEngine;

public class InputManager : MonoBehaviour
{
    ItemLoader itemStats;
    float inventoryWidth = 0.6f;
    float percentFromTopOfScreen = 0.8f;
    // ThirdPersonCharacter player;
    public GameObject obj;
    // Use this for initialization
    void Start()
    {
        // player = GetComponent<ThirdPersonCharacter>();
    }


    // Update is called once per frame
    void Update()
    {

        InterfaceManager.Instance.openCloseInterface(KeyCode.I, "Inventory", inventoryWidth, percentFromTopOfScreen, Inventory.Instance.openInventory);
        InterfaceManager.Instance.openCloseInterface(KeyCode.O, "Bank", 0.1f, 0.8f, Bank.Instance.openBank);
        InterfaceManager.Instance.openCloseInterface(KeyCode.E, "EquipmentTab", 0.1f, 0.8f, Equipment.Instance.openTab);

        if (Input.GetKeyDown(KeyCode.U))
            Inventory.Instance.addItem("Gold", 10);
        if (Input.GetKeyDown(KeyCode.M))
        {
            Inventory.Instance.addItem("Leather Boots", 1);
            Inventory.Instance.addItem("Leather Helm", 1);
            Inventory.Instance.addItem("Leather Chest", 1);
            Inventory.Instance.addItem("Leather Gloves", 1);
            Inventory.Instance.addItem("Leather Pants", 1);
            Inventory.Instance.addItem("Shield", 1);
            Inventory.Instance.addItem("Bottoms", 1);
            Inventory.Instance.addItem("sword_epic", 1);
        }

        if (Input.GetKeyDown(KeyCode.Q))
        {
            CreatePlayer.AddLimb(obj, gameObject, 4);
            Debug.Log("hey");
        }

        if (Input.GetButtonDown("Fire2"))
            GetComponent<Melee>().combo();

        if (Input.GetKeyDown(KeyCode.J))
            Equipment.Instance.removeItem(0);
    }

}
