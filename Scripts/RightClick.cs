﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class RightClick : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{

    // Use this for initialization
    private bool hover;
    GameObject rightClickMenu = null;
    // Update is called once per frame
    void Update()
    {
        if (hover)
        {
            if (Input.GetMouseButtonDown(1))
            {
                openMenu(10);
            }
        }
    }

    private void openMenu(int buttonAmm)
    {

        if (rightClickMenu)
        {
            closeMenu();
        }
        gameObject.transform.SetAsLastSibling();
        GameObject menuParent = new GameObject();
        menuParent.transform.SetParent(gameObject.transform);
        menuParent.transform.localPosition = new Vector3(0f, 0f, 0f);
        rightClickMenu = menuParent;


        for (var i = 0; i < buttonAmm; i++)
        {
            var text = (GameObject)Instantiate(Resources.Load("Interfaces/Button"), new Vector2(0f, 0f), new Quaternion());
            text.transform.SetParent(rightClickMenu.transform);
            text.transform.localPosition = new Vector3(0f, -20f * i, 0f);
            text.GetComponent<RectTransform>().sizeDelta = new Vector2(95, 20);
            if (Bank.Instance.bank() && Bank.Instance.bank().activeInHierarchy)
            {
                if (gameObject.transform.parent.name == "Bank")
                {
                    switch (i)
                    {
                        case 0:
                            text.GetComponentInChildren<Text>().text = "Withdraw: 1x";
                            text.GetComponent<Button>().onClick.AddListener(() => { Bank.Instance.itemClicked(gameObject); });

                            break;
                        case 1:
                            text.GetComponentInChildren<Text>().text = "Withdraw: 10x";
                            text.GetComponent<Button>().onClick.AddListener(() => { Bank.Instance.itemClicked(gameObject,10); });
                            break;
                        case 2:
                            text.GetComponentInChildren<Text>().text = "Withdraw: 28x";
                            text.GetComponent<Button>().onClick.AddListener(() => { Bank.Instance.itemClicked(gameObject,28); });
                            break;
                        case 3:
                            text.GetComponentInChildren<Text>().text = "Withdraw: All";
                            text.GetComponent<Button>().onClick.AddListener(() => { Bank.Instance.itemClicked(gameObject,gameObject.GetComponent<ItemData>()._Amount); });
                            return;
                    }

                }
                else
                {

                    switch (i)
                    {
                        case 0:
                            text.GetComponentInChildren<Text>().text = "Bank: 1x";
                            text.GetComponent<Button>().onClick.AddListener(() => { Inventory.Instance.itemClicked(gameObject); });

                            break;
                        case 1:
                            text.GetComponentInChildren<Text>().text = "Bank: 10x";
                            text.GetComponent<Button>().onClick.AddListener(() => { Inventory.Instance.itemClicked(gameObject,10); });
                            break;
                        case 2:
                            text.GetComponentInChildren<Text>().text = "Bank: All";
                            text.GetComponent<Button>().onClick.AddListener(() => { Inventory.Instance.itemClicked(gameObject, int.MaxValue); });
                            return;
                    }
                }
            }
            else
            {
                /* if (UIManager.instance.gameObject.GetComponent<ItemOnItem>().selected)
                 {
                     switch (i)
                     {
                         case 0:
                             text.GetComponentInChildren<Text>().text = "Use With";
                            // text.GetComponent<Button>().onClick.AddListener(() => { UIManager.instance.gameObject.GetComponent<UIManager>().useItem(1); });
                             return;
                     }
                 }
                 else
                 {*/

                if (gameObject.tag == "Item")
                {
                    switch (i)
                    {
                        case 0:
                            text.GetComponentInChildren<Text>().text = "Select";
                            // text.GetComponent<Button>().onClick.AddListener(() => { UIManager.instance.gameObject.GetComponent<ItemOnItem>().selectItem(gameObject); });
                            break;
                        case 1:
                            text.GetComponentInChildren<Text>().text = "Drop";
                            //  text.GetComponent<Button>().onClick.AddListener(() => { UIManager.instance.gameObject.GetComponent<UIManager>().dropitem(gameObject); });
                            return;
                    }
                }
                else if (gameObject.tag == "Weapon")
                {
                    switch (i)
                    {
                        case 0:
                            text.GetComponentInChildren<Text>().text = "Equip";
                            // text.GetComponent<Button>().onClick.AddListener(() => { UIManager.instance.gameObject.GetComponent<UIManager>().useItem(1); });
                            break;
                        case 1:
                            text.GetComponentInChildren<Text>().text = "Select";
                            //  text.GetComponent<Button>().onClick.AddListener(() => { UIManager.instance.gameObject.GetComponent<ItemOnItem>().selectItem(gameObject); });
                            break;
                        case 2:
                            text.GetComponentInChildren<Text>().text = "Drop";
                            //   text.GetComponent<Button>().onClick.AddListener(() => { UIManager.instance.gameObject.GetComponent<UIManager>().dropitem(gameObject); });
                            return;
                    }
                }
                else if (gameObject.tag == "Food")
                {
                    switch (i)
                    {
                        case 0:
                            text.GetComponentInChildren<Text>().text = "Eat";
                            // text.GetComponent<Button>().onClick.AddListener(() => { UIManager.instance.gameObject.GetComponent<UIManager>().useItem(1); });
                            break;
                        case 1:
                            text.GetComponentInChildren<Text>().text = "Select";
                            // text.GetComponent<Button>().onClick.AddListener(() => { UIManager.instance.gameObject.GetComponent<ItemOnItem>().selectItem(gameObject); });
                            break;
                        case 2:
                            text.GetComponentInChildren<Text>().text = "Drop";
                            // text.GetComponent<Button>().onClick.AddListener(() => { UIManager.instance.gameObject.GetComponent<UIManager>().dropitem(gameObject); });
                            return;
                    }
                }
            }
            //}
        }
    }
    public void OnPointerEnter(PointerEventData clicked)
    {
        hover = true;
    }
    public void OnPointerExit(PointerEventData clicked)
    {
        hover = false;
        closeMenu();
    }
    public void closeMenu()
    {
        hover = false;
        if (rightClickMenu)
            Destroy(rightClickMenu);
    }
}