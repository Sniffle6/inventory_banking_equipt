﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Misc : MonoBehaviour  {

    public static void createText(Transform parent, Vector3 localPos, int stacks)
    {
        var clone = (GameObject)Instantiate(Resources.Load("Text/Text"), new Vector3(0f, 0f, 0f), Quaternion.Euler(Vector3.zero));
        clone.transform.SetParent(parent);
        clone.transform.localPosition = localPos;
        clone.GetComponent<Text>().text = "" + stacks;
    }

}
